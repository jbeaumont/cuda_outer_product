#ifndef SORTED_LIST_H
#define SORTED_LIST_H


#include "globals.h"

typedef char PTR_TYPE;

// reaaaaaally bad for SIMT... other threads just sit around doing nothing after finishing
// also... pointer chasing galore
class sorted_list_t {
public:
    __device__ sorted_list_t(double *s, int entries, int threads_per_block) {
      head = threadIdx.x;
      tail = threadIdx.x;
      almost_tail = -1;
      filled = false;
      num_entries = entries;
      block_size = threads_per_block;
      
//      stuff stored in SP
      vals = s;
      col_idxs = (int*)&vals[num_entries*block_size];
      list_els = (list_element*)&col_idxs[num_entries*block_size];
      track_iter = (int*)&list_els[num_entries*block_size];
      ll_ptrs = (char*)&track_iter[num_entries*block_size];
      cur_size = 0;
    };
    __device__ bool empty() {
        return cur_size == 0;
    }
    __device__ void print_buffer() {
        printf("[%d,%d] Logical layout: (%d)\n", blockIdx.x, threadIdx.x, cur_size);
        cudaSafecall(cudaDeviceSynchronize());
        int count=0;
        for(int i=head; i!=tail; i=ll_ptrs[i]) {
            printf("\t[%d,%d] [%d] PP%d: %d->%f\n", blockIdx.x, threadIdx.x, i, list_els[i].pp_id, col_idxs[i], vals[i]);
            cudaSafecall(cudaDeviceSynchronize());
            count++;
            if(count > num_entries) break;
        }
        cudaSafecall(cudaDeviceSynchronize());
    };
    
    
    __device__ void pop_to_memory(CSR *final_result, int result_idx, list_element &el, int &iter){
        if(cur_size <= 0) {
            printf("Error, trying to pop element from empty buffer\n");
            cudaSafecall(cudaDeviceSynchronize());
            asm("trap;");
        }
        final_result->vals[result_idx] = vals[head];
        final_result->col_idxs()[result_idx] = col_idxs[head];
        el = list_els[head];
        iter = track_iter[head];
//        printf("[%d,%d] Writing %d->%f(%d) to index %d\n",
//                blockIdx.x, threadIdx.x, col_idxs[head], vals[head], el.pp_id, result_idx);
//        cudaSafecall(cudaDeviceSynchronize());
        head = ll_ptrs[head];
        cur_size--;
        return;
    };
    
    
    __device__ void insert(double insert_val, int insert_col, list_element insert_el, int insert_iter, bool &merged){
        if(cur_size >= num_entries) {
            printf("Error, trying to insert element into full buffer\n");
            cudaSafecall(cudaDeviceSynchronize());
            asm("trap;");
        }
//        printf("[%d,%d] PP %d inserting [%d->%f]\n", blockIdx.x, threadIdx.x, insert_el.pp_id, insert_col, insert_val);
//        cudaSafecall(cudaDeviceSynchronize());
        PTR_TYPE itr_idx = head;
        PTR_TYPE *prev_idx = &head;
        merged = false;
        for(int i=0; i<num_entries; i++) {
            int test_col_idx = col_idxs[itr_idx];
            if(itr_idx == tail | insert_col < test_col_idx) {
//                printf("\t[%d,%d] Inserting to element %d\n", blockIdx.x, threadIdx.x, itr_idx);
//                cudaSafecall(cudaDeviceSynchronize());
                // insert element
                vals[tail] = insert_val;
                col_idxs[tail] = insert_col;
                list_els[tail] = insert_el;
                track_iter[tail] = insert_iter;
                *prev_idx = tail;
                char new_tail = tail + block_size;
                if(itr_idx == tail) {
                    // insert at end of list, update pointer to new tail
                    ll_ptrs[tail] = new_tail;
                    almost_tail = tail;
                } else {
                    // insert before end of list, update pointer to current element
                    ll_ptrs[tail] = itr_idx;
                    // ...and update old tail to new tail
                    ll_ptrs[almost_tail] = new_tail;
                    
                }
                cur_size++;
                tail = new_tail;
                if(cur_size >= num_entries) {
                    printf("\t[%d,%d] Run out of entries (%d >= %d)\n", blockIdx.x, threadIdx.x, cur_size, num_entries);
                    cudaSafecall(cudaDeviceSynchronize());
                }
                return;
            } else if(insert_col > test_col_idx){
//                printf("\t[%d,%d] Bigger than element %d\n", blockIdx.x, threadIdx.x, itr_idx);
//                cudaSafecall(cudaDeviceSynchronize());
                //keep reference to this list pointer incase we need to change it
                //(if we insert the value into the next element)
                prev_idx = &ll_ptrs[itr_idx];
                //chase the pointer
                itr_idx = ll_ptrs[itr_idx];
                continue;
            } else { // insert_col == test_idx                
//                printf("\t[%d,%d] Merging with element %d\n", blockIdx.x, threadIdx.x, itr_idx);
//                cudaSafecall(cudaDeviceSynchronize());
                // merge elements
                vals[itr_idx] += insert_val;
                merged = true;
                return;
            }
        }
        printf("Uh-oh, something wrong in merge phase\n");
        cudaSafecall(cudaDeviceSynchronize());
        asm("trap;");
        
    };
private:
    double *vals;
    int *col_idxs;
    list_element *list_els;
    int *track_iter;
//    int *pp_ids;
    PTR_TYPE *ll_ptrs;
    
    PTR_TYPE head;
    PTR_TYPE tail; // points after the last element
    PTR_TYPE almost_tail; // points to the last element
    int block_size;
    int num_entries;
    bool filled;
    int cur_size;
};




#endif

