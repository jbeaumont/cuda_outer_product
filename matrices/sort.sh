#!/bin/bash
FILES=*.mtx
declare -i lines a b
for f in $FILES
do
	echo "$f"
	lines=$(grep -c "%" $f)
	a=$lines+1
	b=$lines+2
	(head -n $a $f && tail -n +$b  $f | sort -k1 -k2 -n) > tmp
	mv tmp $f
done 
