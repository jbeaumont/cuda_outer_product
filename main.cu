#include <stdio.h>
#include <stdlib.h>
#include <cassert>
#include <iostream>

#include "custom/timer.h"

#include "globals.h"
#include "mtx.h"


__global__ void mult_phase(CSC *A, CSR *B, mult_res_t *result, int sp_entries, int num_threads)
{
 
    // each TB grabs pointer to particular row/col pair
    int tid = threadIdx.x;    
    int bid = blockIdx.x;
    
    int col_ptr = A->col_ptrs()[bid];
    int col_size = A->col_ptrs()[bid+1]-col_ptr;
    
    
    int row_ptr = B->row_ptrs()[bid];
    int row_size = B->row_ptrs()[bid+1] - row_ptr;
    
    
    //probably not necessary, but to make debugging easier
    if(row_size*col_size == 0)
        return;
    
    
    // each TB shares partial product
    prtl_csr *prtl_prod = &result->prtl_prods[bid];
    // thread zero initializes partial product pointers
    if(tid == 0) {
        prtl_prod->init(result, col_size, row_size, bid);
    }
    
    
    // scratchpad holds row values and columns
    extern __shared__ double s[];
    double *row_vals = s;
    int    *row_cols = (int*)&row_vals[row_size];
    if(row_size > sp_entries)
        row_cols = (int*)&row_vals[sp_entries];
    
    
    int elements_left = row_size;
    
    
    // keep track of offsets from previous SP load iterations
    int total_offset = 0;
    // all threads iterate for each tile of row data we can fit in scratchpad
    // we'll do multiple iterations if row doesn't fit in SP
    while(elements_left > 0) {
    
        // if scratchpad is too small, only load in subset
        int eff_x_dim = (elements_left > sp_entries) ? sp_entries : elements_left;

        // populate SP
        for(int i=tid; i<eff_x_dim; i+=num_threads) {
            row_vals[i] = B->vals[row_ptr + i + total_offset];
            row_cols[i] = B->col_idxs()[row_ptr + i + total_offset];
        }

        __syncthreads();
        
        // since we might not have enough threads to handle each column value
        // individually, have each thread stride across possible column values
        for(tid = threadIdx.x; tid < col_size; tid += num_threads){
            // TODO: this has bad performance!!! put col_val in SP
            double col_val = A->vals[col_ptr + tid];
            
            //TODO: check for spilling behavior
            if(total_offset==0) {
                int row_idx = A->row_idxs()[col_ptr + tid];
                atomicAdd(&(result->total_row_sizes[row_idx]), row_size);
                list_element el = {.pp_id=bid, .phys_row=tid};
                result->pp_lists[row_idx].push(el);
            }


            // for every row value currently in SP
            for(int i=0; i<eff_x_dim; i++) {
                prtl_prod->set_ele(tid, i+total_offset,
                        col_val * row_vals[i], row_cols[i]);
            }
        }

        elements_left -= eff_x_dim;
        total_offset  += eff_x_dim;
    }
    return;
}

void check_mult(CSC *A, CSR *B, mult_res_t *result)
{
    // for each outer product
    for(int i=0; i<A->num_cols; i++) {
        prtl_csr *prtl_prod = &result->prtl_prods[i];
        int out_idx = 0;
        int spill_idx = 0;
        
        int col_ptr = A->col_ptrs()[i];
        int col_size = A->col_ptrs()[i+1]-col_ptr;
        
        int row_ptr = B->row_ptrs()[i];
        int row_size = B->row_ptrs()[i+1] - row_ptr;
        
        //TODO: implement spillover code
        if(prtl_prod->too_big) {
            printf("Spillover\n");
        }
        
        // for each column value
        for(int j=0; j<col_size; j++) {
            
            
            double a_val = A->vals[col_ptr + j];
            for(int k=0; k<row_size; k++) {
                double b_val = B->vals[row_ptr + k];
                int b_col = B->col_idxs()[row_ptr + k];
                double prod = a_val*b_val;
                double val;
                double *val_addr;
                int col;
                if(prtl_prod->too_big && k >= prtl_prod->max_row_size) {
                    val_addr = &prtl_prod->vals_spill[spill_idx];
                    
                    val = prtl_prod->vals_spill[spill_idx];
                    col = prtl_prod->col_idxs_spill[spill_idx];
                    spill_idx++;
                } else {
                    val_addr = &prtl_prod->vals[out_idx];
                    
                    val = prtl_prod->vals[out_idx];
                    col = prtl_prod->col_idxs[out_idx];
                    out_idx++;
                }
                    
                if(val != prod) {
                    printf("Error in [%d,%d,%d] (0x%p): %f x %f != %f\n",
                            i, j, k, val_addr, a_val, b_val, val);
//                    exit(1);
                } if(col != b_col) {
//                    printf("Error in OP %d [%d,%d]: index marked as %d\n",
//                            i, j, k, col);
//                    exit(1);
                }
            }
        }
    }
}

#include "sorted_list.h"


// newer implementation to match custom architecture algorithm
// many small TBs, serially merging rows from different partial products
// (original implementation parallelized merging different partial products)
// this will severely underutilize hardware when matrix rank is < 20K,
// but shouldn't be an issue for target workloads
// does not SIMT-ize very well...
__global__ void merge_phase(mult_res_t *mult_result, CSR *final_result, int block_size,
        int num_entries)
{
    cudaSafecall(cudaDeviceSynchronize());
    
    int row_idx = blockIdx.x*block_size + threadIdx.x;
    if(row_idx >= mult_result->dimension) {
        return;
    }
    pp_list_t *pp_list = &mult_result->pp_lists[row_idx];
    int num_PPs = pp_list->get_size();
    if(num_PPs > num_entries) {
        printf("Uh-oh... issue with overspill... increase SM space (or add patch code)\n");
        cudaSafecall(cudaDeviceSynchronize());
        asm("trap;");
    }
//    int total_size = mult_result->total_row_sizes[row_idx];
    
    
    extern __shared__ double s[];
    
    sorted_list_t list(s,num_entries, block_size);
        
//    first, read first element from each sublist into sorted local memory
    int iter=0;
    for(int i=0; i<num_PPs; i++) {
        list_element el = pp_list->read(i);
        int pp_id = el.pp_id;
        int phys_row = el.phys_row;
      
        prtl_csr *prtl_prod = &mult_result->prtl_prods[pp_id];
        int row_size = prtl_prod->row_size;
        
        
        bool merged, row_done;
        do {
            int col_idx = prtl_prod->col_idxs[phys_row*row_size+iter];
            double val = prtl_prod->vals[phys_row*row_size+iter];
            iter++;
            list.insert(val, col_idx, el, iter, merged);
            row_done = iter>=row_size;
        } while(merged && !row_done);
    }
    
    
    int write_iter = 0;
    while(!list.empty()) {
    //    write element
        int result_idx = (final_result->row_ptrs()[row_idx])+write_iter;
        write_iter++;
        list_element el;
        list.pop_to_memory(final_result, result_idx, el, iter);

    //    then, read next elements

    //    TODO: copy pasted code... gross, move to function    

        int pp_id = el.pp_id;
        int phys_row = el.phys_row;

        prtl_csr *prtl_prod = &mult_result->prtl_prods[pp_id];
        int row_size = prtl_prod->row_size;


        bool merged = true;
        bool row_done = (iter >= row_size);
        while(merged && !row_done) {
            int col_idx = prtl_prod->col_idxs[phys_row*row_size+iter];
            double val = prtl_prod->vals[phys_row*row_size+iter];
            iter++;
            list.insert(val, col_idx, el, iter, merged);
            row_done = iter>=row_size;
        }
    }
        
    
    
//    for(int i=0; i<block_size; i++) {
//        if(threadIdx.x == i)
//            list.print_buffer();
//    }
    
    
    return;

}


int main(int argc, char **argv)
{
    // Assumptions:
    //	* Input file is in Matrix Market format
    //	* Each row in input file is fewer than 10,000 characters in size
    //  * Fewer than 4 billion elements (so pointers can fit in "int")

    int dev = 0;
    cudaSafecall(cudaSetDevice(dev));
    cudaDeviceProp deviceProp;
    cudaSafecall(cudaGetDeviceProperties(&deviceProp, dev));
    
//    cudaSafecall(cudaDeviceSetLimit(cudaLimitMallocHeapSize, 256 * (1 << 20)));
    
    if(argc != 2) {
        printf("Usage: %s <MatrixMarketFile.mtx>\n", argv[0]);
        exit(1);
    }
    
    const char *file;
    
    
    
    file = argv[1];
    
    CSR *B;
    cudaSafecall(cudaMallocManaged(&B, sizeof(CSR)));
    printf("Reading in matrix...\n");
    B = new (B) CSR(file);
    B->prefetch_to_device(dev);
    
    CSC *A;
    cudaSafecall(cudaMallocManaged(&A, sizeof(CSC)));
    
    printf("Converting to CSC...\n");
    A = new (A) CSC(B);
    A->prefetch_to_device(dev);    
    
    mult_res_t *multiply_result;
    cudaSafecall(cudaMallocManaged(&multiply_result, sizeof(mult_res_t)));
    
    int avg_col_size = ceil((double)A->num_vals / A->num_cols);
    // how much to scale average storage requirement by for typical storage
    const int MULT_SCALE_PARAM = 8;
    const int MAX_SM = 49152;
    
    // number of elements to store in non-spill space
    int num_output_els = MULT_SCALE_PARAM * avg_col_size * avg_col_size;
    int num_sm_entries = MULT_SCALE_PARAM * avg_col_size;  
    int sm_size = num_sm_entries * (sizeof(double) + sizeof(int));
    if(sm_size > MAX_SM) {
        printf("ERROR: too much SM\n");
        exit(1);
    }
    
    int spill_tiles = MULT_SCALE_PARAM * A->num_vals / num_output_els; 
    
    
    printf("Initializing result structure...\n");
    multiply_result = new (multiply_result) mult_res_t(A->num_rows, num_output_els,
            spill_tiles);
    
    multiply_result->prefetch_to_device(dev);
    
    cudaDeviceSetLimit(cudaLimitPrintfFifoSize, 100000000);
  
    GpuTimer timer;
    printf("Starting mult...\n");
    int num_threads = num_sm_entries;
    timer.Start();
    mult_phase<<<A->num_cols, num_threads, sm_size>>>(A,B,multiply_result, num_threads, num_sm_entries);
    cudaSafecall(cudaDeviceSynchronize());
    timer.Stop();
    std::cout<<timer.Elapsed()<<std::endl;
    printf("Done with multiply\n");

//    check_mult(A,B,multiply_result);
    
    
    CSR *final_result;
    cudaSafecall(cudaMallocManaged(&final_result, sizeof(CSR)));
    final_result = new (final_result) CSR(multiply_result);
    int avg_row_size = final_result->num_vals / final_result->num_rows; //average assuming no merges
    //TODO: this redundantly sends data and indices across (which aren't
    // initialized)... but hopefully after the useful stuff is
    final_result->prefetch_to_device(dev);
    
    const int MERGE_SCALE_PARAM = 16;
    num_sm_entries = MERGE_SCALE_PARAM * avg_row_size;
    // In each entry in scratchpad, we'll hold the value, col idx,
    // [partial product ID + physical row], tracking index, and pointer to next element (for linked list)
    int sm_size_per_thread = num_sm_entries * (sizeof(double) + sizeof(int) +
        sizeof(list_element) + sizeof(int) + sizeof(PTR_TYPE));
    
//    int block_size = min(32, A->num_cols);
    int block_size = 1;
    
    
    sm_size = sm_size_per_thread * block_size;
    if(sm_size > MAX_SM) {
        printf("ERROR: too much SM\n");
        exit(1);
    }
    int num_blocks = (A->num_cols-1) / block_size + 1;
    
    printf("Starting merge...\n");
    merge_phase<<<num_blocks, block_size, sm_size>>>(
            multiply_result, final_result, block_size, num_sm_entries);
    cudaSafecall(cudaPeekAtLastError());
    cudaSafecall(cudaDeviceSynchronize());
    
    printf("Done with merge\n");
    
//    printf("Final result\n");
//    final_result->print();
    
}