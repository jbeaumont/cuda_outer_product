#ifndef GLOBALS_H
#define GLOBALS_H

#define cudaSafecall(call)                                  \
{                                                           \
  cudaError_t cucheck_err = (call);                         \
  if(cucheck_err != cudaSuccess) {                          \
    const char *err_str = cudaGetErrorString(cucheck_err);  \
    printf("%s (%d): %s (%d)\n", __FILE__, __LINE__, err_str, cucheck_err);   \
    /*assert(0);*/                                              \
  }                                                         \
}


#endif /* GLOBALS_H */

