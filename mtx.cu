#include "mtx.h"


void CSR::CSR_dep(const char *file){
    
    FILE *fp = fopen(file, "r");
    
    name = "CSR";
    char line[10000];

    // Get graph dimensions
    while(fgets(line, sizeof line, fp)) {
        if(*line == '%') continue;
        if(sscanf(line, "%d %d %d", &num_rows, &num_cols, &num_vals) != 3) {
            printf("Problem with format of input file");
            exit(1);
        }
        break;
    }
    
    int *ptrs, *idxs;
    cudaSafecall(cudaMallocManaged(&ptrs, sizeof(int)*num_rows+1));
    cudaSafecall(cudaMallocManaged(&idxs, sizeof(int)*num_vals));
    
    set_row_ptrs(ptrs);
    set_col_idxs(idxs);

    cudaSafecall(cudaMallocManaged(&vals, sizeof(double)*num_vals));

    int rows[num_vals];
    ptrs[0] = 0;

    int last_row = 0;
    int row_size = 0;
    
    max_row_size = 0;

    // read contents of file
    int i;
    for(i=0; i<num_vals; i++) {
        // read in an entry (one line, ignoring comments)
        do {
            if(fgets(line, sizeof line, fp) == NULL) {
                printf("Error: wrong number of rows\n");
                exit(1);
            }
        } while(*line == '%'); // ignore lines starting with %
        if(sscanf(line, "%d %d %lf", &rows[i], &col_idxs()[i], &vals[i]) != 3) {
            printf("Problem with format of input file");
            exit(1);
        }
        // MM format starts index at 1, not 0
        rows[i]--;
        col_idxs()[i]--;

        // check for illegal values
        if(rows[i] >= num_rows || col_idxs()[i] >= num_cols) {
            printf("Error: row / col index too large\n");
            exit(1);
        }
        if(rows[i] < last_row) {
            printf("Error: non-monotonic row indexes\n");
            exit(1);
        } else if(rows[i] > last_row) {
        // new row: add entries to row_ptrs
            int j;
            for(j=last_row; j<rows[i]; j++) {
                row_ptrs()[j+1] = row_ptrs()[j] + row_size;
                max_row_size = max(max_row_size, row_size);
                row_size = 0;
            }
        }
        row_size++;
        last_row = rows[i];
    }

    // add any remaining entries to row_ptrs
    row_ptrs()[last_row+1] = row_ptrs()[last_row] + row_size;
    max_row_size = max(max_row_size, row_size);
    int j;
    for(j=last_row+1; j<num_rows; j++) {
            row_ptrs()[j+1] = row_ptrs()[j];
    }

    // sanity check on number of values
    if(row_ptrs()[num_rows] != num_vals) {
            printf("Error: something wrong with row_ptrs (%d != %d)\n", row_ptrs()[num_rows+1], num_vals);
            exit(1);
    }
    // TODO: why doesn't this work?
    max_row_size *= 2;
}

void compr_mtx::print() {
    printf("%s (%d,%d,%d)\n", name, num_rows, num_cols, num_vals);

    printf("Vals =");
    int i;
    for(i=0; i<num_vals; i++) {
            printf(" %f", vals[i]);
    }
    printf("\n");

    printf("ptrs =");
    for(i=0; i<num_rows+1; i++) {
            printf(" %d", ptrs[i]);
    }
    printf("\n");

    printf("idxs =");
    for(i=0; i<num_vals; i++) {
            printf(" %d", idxs[i]);
    }
    printf("\n\n");
}

CSC::CSC(const CSC& csc) {
    name = "CSC";

    num_rows = csc.num_rows;
    num_cols = csc.num_cols;
    num_vals = csc.num_vals;
    vals     = csc.vals;
    
    max_row_size = csc.max_row_size;
    max_col_size = csc.max_col_size;

    set_col_ptrs(csc.col_ptrs());
    set_row_idxs(csc.row_idxs());
	
}


CSR::CSR(const CSR& csr) {
    name = "CSR";

    num_rows = csr.num_rows;
    num_cols = csr.num_cols;
    num_vals = csr.num_vals;
    vals     = csr.vals;
    
    max_row_size = csr.max_row_size;

    set_row_ptrs(csr.row_ptrs());
    set_col_idxs(csr.col_idxs());
}

CSR::CSR(const mult_res_t *mul_res) {
    name = "CSR";
    
    num_rows = mul_res->dimension;
    num_cols = mul_res->dimension;
    int *ptrs, *idxs;
    cudaSafecall(cudaMallocManaged(&ptrs, sizeof(int)*num_rows+1));
    //TODO: parallel prefix sum this
    // (currently a small overhead for tested workloads)
    num_vals=0;
//    printf("Final result storage\n");
    for(int i=0; i<mul_res->dimension; i++) {
        ptrs[i] = num_vals;
        num_vals += mul_res->total_row_sizes[i];
        
//        printf("Row %d has %d elements\n", i, num_vals);
    }
//    printf("Total of %d elements\n", num_vals);
    cudaSafecall(cudaMallocManaged(&idxs, sizeof(int)*num_vals));
    
    set_row_ptrs(ptrs);
    set_col_idxs(idxs);
    
    cudaSafecall(cudaMallocManaged(&vals, sizeof(double)*num_vals));
    
    
    
}

CSC::CSC(CSR *csr) {
    name = "CSC";

    num_rows = csr->num_rows;
    num_cols = csr->num_cols;
    num_vals = csr->num_vals;
    
    
    cudaSafecall(cudaMallocManaged(&vals, sizeof(double)*num_vals));
    
    max_row_size = csr->max_row_size;
    
    
    int *ptrs, *idxs;
    cudaSafecall(cudaMallocManaged(&ptrs, sizeof(int)*num_cols+1));
    cudaSafecall(cudaMallocManaged(&idxs, sizeof(int)*num_vals));

    set_col_ptrs(ptrs);
    set_row_idxs(idxs);

//    int row_trackers[csr->num_rows];
    int *row_trackers = new int[csr->num_rows];
    for(int row_idx=0; row_idx<csr->num_rows; row_idx++) {
        row_trackers[row_idx] = 0;
    }

    col_ptrs()[0] = 0;

    int csc_idx = 0;
    for(int col_idx=0; col_idx<num_cols; col_idx++) {
        for(int row_idx=0; row_idx<num_rows; row_idx++) {
            int ptr = csr->row_ptrs()[row_idx]+row_trackers[row_idx];
            if(ptr == csr->row_ptrs()[row_idx+1]) continue;
            if(csr->col_idxs()[ptr] == col_idx) {
                vals[csc_idx] = csr->vals[ptr];
                row_trackers[row_idx]++;
                row_idxs()[csc_idx] = row_idx;
                if(++csc_idx == num_vals) {
                    while(col_idx<num_cols) {
                        col_ptrs()[col_idx+1] = csc_idx;
                        max_col_size = max(max_col_size, col_ptrs()[col_idx+1]-col_ptrs()[col_idx]);
                        col_idx++;
                    }
                    return;
                }
            } else if(csr->col_idxs()[ptr] < col_idx) {
                printf("Something wrong with CSR to CSC ([%d,%d] - Ptr: %d Col_idx: %d\n", row_idx, col_idx, ptr, csr->col_idxs()[ptr]);
                 exit(1);
            }
        }
        col_ptrs()[col_idx+1] = csc_idx;
        max_col_size = max(max_col_size, col_ptrs()[col_idx+1]-col_ptrs()[col_idx]);
    }
    delete row_trackers;
    return;
}

void compr_mtx::prefetch_to_device(int dev) {
    cudaSafecall(cudaMemPrefetchAsync(this, sizeof(this), dev, NULL));
    cudaSafecall(cudaMemPrefetchAsync(ptrs, sizeof(int)*(num_rows+1),dev, NULL));
    cudaSafecall(cudaMemPrefetchAsync(idxs, sizeof(int)*num_vals,    dev, NULL));
    cudaSafecall(cudaMemPrefetchAsync(vals, sizeof(double)*num_vals, dev, NULL));
}

__device__ void prtl_csr::init( mult_res_t *result, int col_size, 
                                int row_size, int block_id) {

    id = block_id;
    vals     = &result->vals    [result->tile_size*block_id];
    col_idxs = &result->col_idxs[result->tile_size*block_id];
    row_idxs = &result->row_idxs[result->tile_size*block_id];
    
    this->row_size = row_size;
    this->col_size = col_size;
    
    max_row_size = result->tile_size / col_size;
    if(max_row_size == 0) {
        printf("Error: more rows than output space - unsupported\n");
        asm("trap;");
    }
    
    if(row_size > max_row_size) {
        too_big = true;
        unsigned int spill_blocks = row_size*col_size / result->tile_size;
        unsigned int tile_idx = atomicAdd(result->spill_pointer, spill_blocks);
        
        if(spill_blocks == 0)
            printf("Hmmm\n");
                
        
        if(tile_idx + spill_blocks > result->num_spill_tiles) {
            printf("Error: spill space overflow");
            asm("trap;");
        }
        unsigned int offset = tile_idx * result->tile_size;
        vals_spill     = &result->vals_spill[offset];
        col_idxs_spill = &result->col_idxs_spill[offset];
        row_idxs_spill = &result->row_idxs_spill[offset];
        
        printf("OP: %d Spill space: 0x%p - 0x%p\n", id, vals_spill, vals_spill+spill_blocks*result->tile_size);
    }
        
}

__device__ void prtl_csr::set_ele( int row, int col, 
                                            double val, int col_idx)
{
    int eff_row_size = min(row_size, max_row_size);
    if(col < max_row_size) {
        int idx = row*eff_row_size + col;
        vals[idx] = val;
        col_idxs[idx] = col_idx;
//        printf("[%u,%d,%d] Writing %f to 0x%p\n", blockIdx.x, row,
//                col, val, &vals[idx]);
        cudaSafecall(cudaDeviceSynchronize());
    } else {
        int idx = row*(row_size-max_row_size) + col-max_row_size;
        vals_spill[idx] = val;
        col_idxs_spill[idx] = col_idx;
//        printf("[%u,%d,%d] Writing %f to 0x%p [spill]\n", blockIdx.x, row,
//                col, val, &vals_spill[idx]);
        cudaSafecall(cudaDeviceSynchronize());
    }
}

#include <iostream>




mult_res_t::mult_res_t(int dim, int tile_size,
        int num_tiles) {
    
    // there are N partial products for NxN matrix
    cudaSafecall(cudaMallocManaged(&prtl_prods, sizeof(prtl_csr) * dim));
    
    // todo: come up with more optimized way of allocating size
    int max_size = dim;
    cudaSafecall(cudaMallocManaged(&pp_lists, sizeof(pp_list_t) * dim));
    cudaSafecall(cudaMallocManaged(&total_row_sizes, sizeof(int) * dim));
    cudaSafecall(cudaMemset(total_row_sizes, 0, sizeof(int) * dim));
    for(int i=0; i<dim; i++) {
        new (&pp_lists[i]) pp_list_t(max_size);
    }
    dimension = dim;
    
    
    this->tile_size = tile_size;
    
    //TODO: don't need this many row_idxs, right? just one per row?
    num_spill_tiles = num_tiles;
    
    cudaSafecall(cudaMallocManaged(&vals,     sizeof(double) * tile_size));
    cudaSafecall(cudaMallocManaged(&col_idxs, sizeof(int)    * tile_size));
    cudaSafecall(cudaMallocManaged(&row_idxs, sizeof(int)    * tile_size));
   
    
    cudaSafecall(cudaMallocManaged(&vals_spill,     sizeof(double) * num_tiles * tile_size));
    cudaSafecall(cudaMallocManaged(&col_idxs_spill, sizeof(int)    * num_tiles * tile_size));
    cudaSafecall(cudaMallocManaged(&row_idxs_spill, sizeof(int)    * num_tiles * tile_size));
    
    cudaSafecall(cudaMallocManaged(&spill_pointer, sizeof(int)));
    *spill_pointer = 0;
    
}

void mult_res_t::prefetch_to_device(int dev) {
    for(int i=0; i<dimension; i++) {
        pp_lists[i].prefetch_to_device(dev);
    }
    cudaSafecall(cudaMemPrefetchAsync(pp_lists, sizeof(pp_list_t)*dimension, dev, NULL));
    cudaSafecall(cudaMemPrefetchAsync(this, sizeof(this), dev, NULL));
}

