#ifndef MTX_H
#define MTX_H

#include <stdio.h>
#include <stdlib.h>
#include <cassert>
#include <utility>

#include "device_launch_parameters.h"

#include "globals.h"
#include "global_list.h"

// compressed matrix base class, will be instantiated as CSR or CSC
class compr_mtx {
protected:
    compr_mtx(){}; // to prevent instantiating base class
    int *ptrs;
    int *idxs;
public:

    int num_rows, num_cols;
    int num_vals;
    double *vals;
    const char *name;
    
    int density;
    
    int max_row_size;

    virtual int ptrs_size() = 0;

    void print();
    
    void prefetch_to_device(int dev);

};

class mult_res_t;


class CSR : public compr_mtx {
protected:
    CSR(){};
public:
    CSR(const CSR& csr);
    CSR(const char *file);
    CSR(const mult_res_t *mul_res);
    void CSR_dep(const char *file);
    __device__ __host__ int *row_ptrs()const{return ptrs;}
    void set_row_ptrs(int *rp){ptrs = rp;}
    int ptrs_size(){return num_rows+1;}

    __device__ __host__ int *col_idxs()const{return idxs;}
    void set_col_idxs(int *ci){idxs = ci;}		
		
};

class  CSC : public compr_mtx {
protected:
    CSC(){};
public:
    CSC(const CSC& csc);
    CSC(CSR *csr);
    __device__ __host__ int *col_ptrs()const{return ptrs;}
    void set_col_ptrs(int *cp){ptrs = cp;}
    int ptrs_size(){return num_cols+1;}

    __device__ __host__ int *row_idxs()const{return idxs;}
    void set_row_idxs(int *ri){idxs = ri;}

    
    int max_col_size;
		
};

class prtl_csr {
public:
    __device__ void init(mult_res_t *result, int col_size, int row_size, int block_id);
    __device__ void set_ele(int row, int col, double val, int col_idx);
    int id;
    
    double *vals;
    int *col_idxs, *row_idxs;
    
    bool too_big;
    
    int col_size, row_size;
    
    // maximum number of elements stored in non-spill space
    int max_row_size;
    
    double *vals_spill;
    int *col_idxs_spill, *row_idxs_spill;
    
    // for iterating over elements in merge stage
    int *iters;
    
    
};

class mult_res_t {
public:
    mult_res_t(int dim, int max_rows,
            int spill_tiles);
    
    // contains pointers to data and spill over, and flags
    prtl_csr *prtl_prods;
    
    // for each row, maintain a reference to partial
    // products which contribute to that row
    int dimension;
    pp_list_t *pp_lists;
    int *total_row_sizes;
    
    int tile_size;
    
    //allocate max_size of each
    double *vals;
    int *col_idxs, *row_idxs;
    
    //allocate enough space to hold entire matrix
    unsigned int num_spill_tiles;
    double *vals_spill;
    int *col_idxs_spill, *row_idxs_spill;
    
    int *spill_pointer;
    int *locks;
    
    
    void prefetch_to_device(int dev);
};


#endif