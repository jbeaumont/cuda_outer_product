# NVCC=/usr/local/cuda/bin/nvcc
NVCC=nvcc
OPT=-g -G
#OPT=-O3
FLAGS=$(OPT) -arch=compute_35 -m64

all: main
	
mtx.o: mtx.cu mtx.h
	$(NVCC) -dc $< $(FLAGS)
	
cusp_hooks.o: cusp_hooks.cu mtx.h
	$(NVCC) -I ../ -dc $< $(FLAGS)
	
main.o: main.cu
	$(NVCC) -I ../cub-1.1.1 -dc $< $(FLAGS)

main: main.o mtx.o cusp_hooks.o
	$(NVCC) $^ -o $@ $(FLAGS)
	
	
	
clean:
	rm -f *.o main