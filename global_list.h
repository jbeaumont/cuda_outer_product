#ifndef GLOBAL_LIST_H
#define GLOBAL_LIST_H

#include "globals.h"

typedef struct {
    int pp_id;
    int phys_row;
    void print() {
        printf("[%d,%d]", pp_id, phys_row);
    }
} list_element;

//issues with template... let's just hard code it to structs
//template<class T>
class pp_list_t {
public:
    pp_list_t(int max_size){
        top = -1;
//        data = new T[max_size];
        cudaSafecall(cudaMallocManaged(&data, sizeof(list_element) * max_size));
        capacity = max_size;
    };
    ~pp_list_t() {
        if(data) delete [] data;
    };
    void prefetch_to_device(int dev){
        cudaSafecall(cudaMemPrefetchAsync(data, sizeof(list_element) * capacity, dev, NULL));        
    }
    __device__ int push(list_element e){
        int idx = atomicAdd(&(top), 1);
        if(idx < (capacity-1)) {
            data[idx+1] = e;
            return idx+1;
        } else {
            unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
            printf("%s(%d): thread %d: Error: buffer overflow, capacity=%d.\n", __FILE__, __LINE__, id, capacity);
            cudaDeviceSynchronize();
            return -1;
        }
    };
    __device__ bool pop(list_element &e){
        if(data && top >= 0) {
//            pops don't need to be atomic
//            int idx = atomicAdd(&top, -1);
            int idx = top;
            top--;
            if(idx > 0) {
                e = data[idx];
                return true;
            }
        }
        return false;
    };
    __device__ int get_size() {
        return top+1;
    };
    void print(){
        for(int i=0; i<(top+1); i++) {
            data[i].print();
            printf(" ");
        }
        printf("\n");
    };
    
    __device__ list_element read(int idx) {
      return data[idx];  
    };
    
private:
    int capacity;
    list_element *data;
    int top;
};



#endif /* GLOBAL_LIST_H */

