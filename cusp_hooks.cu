#include "mtx.h"


#include "cusp/io/matrix_market.h"

CSR::CSR(const char *file) {
    cusp::csr_matrix<int, float, cusp::host_memory> A;
    cusp::io::read_matrix_market_file(A, file);
    
    name = "CSR";
    num_rows = A.num_rows;
    num_cols = A.num_cols;
    num_vals = A.num_entries;
    
    
    int *ptrs, *idxs;
    cudaSafecall(cudaMallocManaged(&ptrs, sizeof(int)*num_rows+1));
    cudaSafecall(cudaMallocManaged(&idxs, sizeof(int)*num_vals));
    
    set_row_ptrs(ptrs);
    set_col_idxs(idxs);
    
    cudaSafecall(cudaMallocManaged(&vals, sizeof(double)*num_vals));
    
    for(int i=0; i<=A.num_rows; i++) {
        ptrs[i] = A.row_offsets[i];
    }
    for(int i=0; i<A.num_entries; i++) {
        idxs[i] = A.column_indices[i];
        vals[i] = A.values[i];
    }
}